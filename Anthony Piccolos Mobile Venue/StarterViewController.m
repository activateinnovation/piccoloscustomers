//
//  StarterViewController.m
//  Anthony Piccolos Mobile Venue
//
//  Created by Test on 6/9/14.
//  Copyright (c) 2014 ActivateInnovation. All rights reserved.
//

#import "StarterViewController.h"

@interface StarterViewController ()

@end

@implementation StarterViewController
@synthesize alreadySignedUpOutlet;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    
    UIStoryboard *storyboard;
    if(self.window.frame.size.width > 320){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main-IPad" bundle:nil];
    }
    else{
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"hadFirstLogin"] == NO){
        
       /* NSLog(@"TESTing");
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"starter"];
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];*/
        
    }
    else{
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"mainView"];
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
        
    }
    
    [self.alreadySignedUpOutlet setBackgroundColor:[UIColor lightGrayColor]];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background.png"]];
   // [background setFrame: CGRectMake(0, 0, 320, 568)];
    [background setFrame:self.view.frame];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
   
    MKMapView *mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    mapView.delegate = self;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
    [self.locationManager requestWhenInUseAuthorization];
    [mapView setShowsUserLocation:YES];

   // [self.view addSubview:mapView];
    
  //  NSLog(@"HERE");
    
    
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the vie
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextOnKeyboard:(id)sender {
    
    if(sender == self.firstName){
        
        [self.firstName resignFirstResponder];
        [self.lastName becomeFirstResponder];
        
    }
    else if(sender == self.lastName)
    {
        [self.lastName resignFirstResponder];
        [self.emailAddress becomeFirstResponder];
        
    }
    else{
        
        [self.emailAddress resignFirstResponder];
        
    }
        
    
}

- (IBAction)tappedScreen:(id)sender {
    
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.emailAddress resignFirstResponder];
}

- (IBAction)submitUser:(id)sender {
    
    if(self.firstName.text.length < 2 || self.lastName.text.length < 2 || self.emailAddress.text.length < 2){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please fill out all fields in order to start placing orders." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else{
    //First need to verify that there is no user with that email already
    
        PFQuery *emailQuery = [PFUser query];
        [emailQuery whereKey:@"email" equalTo: self.emailAddress.text];
        [emailQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if(objects.count > 0){
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Duplicate Email" message:@"There is already a user registered with that email. Please choose a different email and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else{
                
                [self sendUserToParse];
                
            }
            
        }];
    
    }
    
}



-(void) sendUserToParse {
    
    PFUser *user = [PFUser user];
    
    int x = arc4random();
    
    user.username = [NSString stringWithFormat:@"%@%@%d", self.firstName.text, self.lastName.text, x];
    user.password = [NSString stringWithFormat:@"%@%@%d", self.firstName.text, self.lastName.text, x];
    user.email = self.emailAddress.text;
    user[@"firstName"] = self.firstName.text;
    user[@"lastName"] = self.lastName.text;
    
    if(self.updateSwitcher.isOn){
        
        
        user[@"emailNotifications"] = @"YES";
    }
    else{
        
        
        user[@"emailNotifications"] = @"NO";
    }

    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            // Hooray! Let them use the app now.
             [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hadFirstLogin"];
            
            UIStoryboard *storyboard;
            if(self.view.frame.size.width > 320){
                
                storyboard = [UIStoryboard storyboardWithName:@"Main-IPad" bundle:nil];
            }
            else{
                
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            }
            
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"mainView"];
            [self presentViewController:viewController animated:YES completion:nil];
            
            
        } else {
            NSString *errorString = [error userInfo][@"error"];
            // Show the errorString somewhere and let the user try again.
            NSLog(@"Error: %@", errorString);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Duplicte Information" message:@"Someone has already signed up with this email address, please try another email or if you have signed up already click the button below to verify you information." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
}

//This skips the sign up initially but will still show up everytime the app opens unitl they add their information
- (IBAction)skipSignUp:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"hadFirstLogin"];
    
    UIStoryboard *storyboard;
    if(self.view.frame.size.width > 320){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main-IPad" bundle:nil];
    }
    else{
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self presentViewController:viewController animated:YES completion:nil];

    
    
}

- (IBAction)alreadySignedUpButton:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Verify Account" message:@"Please enter the email that you signed up with to verify your information." delegate:self cancelButtonTitle:@"Verify" otherButtonTitles: @"Cancel", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert setTag:1];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 1){
        
        
        NSString *email = [alertView textFieldAtIndex:0].text;
        NSLog(@"EMail: %@", email);
        PFQuery *query = [PFUser query];
        [query whereKey:@"email" equalTo:email];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            NSLog(@"Users: %@", objects);

            if(objects.count > 0){
               
                PFUser *user = [objects objectAtIndex:0];
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hadFirstLogin"];
                
                UIStoryboard *storyboard;
                if(self.view.frame.size.width > 320){
                    
                    storyboard = [UIStoryboard storyboardWithName:@"Main-IPad" bundle:nil];
                }
                else{
                    
                    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                }
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Welcome Back" message:[NSString stringWithFormat:@"Welcome back %@ %@ to Anthony Piccolos Mobile Venue!", user[@"firstName"], user[@"lastName"]] delegate:self cancelButtonTitle:@"Continue" otherButtonTitles:nil, nil];
                [alert setTag:1];
                
                UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"mainView"];
                [self presentViewController:viewController animated:YES completion:nil];
                
                [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
                
                
            }
            else{
              
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Found" message:@"The email you provided was not found. Please check the spelling or try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            
        }];
        
    }
    
}
@end

//
//  AppDelegate.h
//  Anthony Piccolos Mobile Venue
//
//  Created by ActivateInnovation on 6/4/14.
//  Copyright (c) 2014 ActivateInnovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//@property (strong,nonatomic) CLLocationManager *locationManager;
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo;

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken;

@property (strong, nonatomic) MainViewController *mainController;

@end

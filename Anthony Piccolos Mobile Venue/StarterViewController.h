//
//  StarterViewController.h
//  Anthony Piccolos Mobile Venue
//
//  Created by Test on 6/9/14.
//  Copyright (c) 2014 ActivateInnovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MainViewController.h"
#import <Parse/Parse.h>

@class MainViewController;

@interface StarterViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate, UIAlertViewDelegate  >
@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *emailAddress;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UISwitch *updateSwitcher;
- (IBAction)nextOnKeyboard:(id)sender;
- (IBAction)tappedScreen:(id)sender;
- (IBAction)submitUser:(id)sender;
- (IBAction)skipSignUp:(id)sender;
- (IBAction)alreadySignedUpButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *alreadySignedUpOutlet;
@property (strong, nonatomic) UIWindow *window;

@end

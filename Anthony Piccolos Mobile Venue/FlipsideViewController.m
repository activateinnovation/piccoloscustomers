//
//  FlipsideViewController.m
//  Anthony Piccolos Mobile Venue
//
//  Created by ActivateInnovation on 6/4/14.
//  Copyright (c) 2014 ActivateInnovation. All rights reserved.
//

#import "FlipsideViewController.h"


@interface FlipsideViewController ()

@end

@implementation FlipsideViewController
@synthesize truckLocationCoord;
@synthesize webView;
@synthesize spinner;
@synthesize activePromos;
@synthesize container;
@synthesize promoView;
@synthesize blackOut;


-(void) viewWillAppear:(BOOL)animated   {
    
    
    self.webView.delegate = self;
    
  
    self.activePromos = [[NSMutableArray alloc] init];
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.spinner.frame = CGRectMake(((self.view.frame.size.width/2) - 25), self.webView.frame.size.height/2, 50, 50);
    [self.view addSubview:self.spinner];
    [self.view bringSubviewToFront:self.spinner];

    [self.spinner startAnimating];
    
   // OAuth realm="http://projects.chhito.com/piccolos/cms/wc-api/v2/orders/",oauth_consumer_key="ck_c58b5c609727c6f9f0dbc8fbca84f107",oauth_signature_method="HMAC-SHA1",oauth_timestamp="1427914287",oauth_nonce="itz4Jo",oauth_version="1.0",oauth_signature="5bLVH3ixA%2Bp%2FoZPMv815Y2w%2BS0M%3D"
   /* oauth_consumer_key="dpf43f3p2l4k3l03",
    oauth_token="nnch734d00sl2jdk",
    oauth_signature_method="HMAC-SHA1",
    oauth_timestamp="137131202",
    oauth_nonce="chapoH",
    oauth_signature="MdpQcU8iPSUjWoN%2FUDMsK2sui9I%3D"*/
    
    [self.webView setBackgroundColor:[UIColor blackColor]];
    [self.webView setOpaque:NO];
    
    NSString *fullURL = [NSString stringWithFormat: @"http://projects.chhito.com/piccolos/cms/"];
   // NSString *basicAuthCredentials = [NSString stringWithFormat:@"username=%@:password=%@", @"ck_c58b5c609727c6f9f0dbc8fbca84f107", @"cs_b7c86b164a06a0b00a347be454e58a98"];
    
   // NSData *nsdata = [basicAuthCredentials
                     // dataUsingEncoding:NSUTF8StringEncoding];
    // Get NSStrinrom NSData object in Base64
   // NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    //NSString *authValue = [NSString stringWithFormat:@"Basic %@", base64Encoded];
   // NSString *authValue = [NSString stringWithFormat:@"Basic %@", @"YWJzb2x1dGU6MjAwMWFic29sdXRl"];
    
    
    NSURL *url = [NSURL URLWithString:fullURL];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    //[requestObj setValue:authValue forHTTPHeaderField:@"Authorization"];

    [self.webView loadRequest:requestObj];
    
    
    [self updateActivePromoCodes];
    [self performSelector:@selector(getActivePromoCodesFromParse) withObject:nil afterDelay:0.25];
    
    if(self.view.frame.size.width > 320){
        
        self.container = [[UIView alloc] initWithFrame:CGRectMake(240, 250, 290, 360)];
        
    }
    else{
        self.container = [[UIView alloc] initWithFrame:CGRectMake(15, 100, 290, 360)];
    }
    
    
    self.container.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.container];
    [self.view sendSubviewToBack:self.container];
    self.promoView = [[UIView alloc] initWithFrame:self.container.bounds];
    self.promoView.clearsContextBeforeDrawing = YES;

   
    
      [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib
    
    
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background.png"]];
    [background setFrame: self.view.frame];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    
    
}


-(void) updateActivePromoCodes {
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"ActivePromoCode"];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [array addObjectsFromArray:objects];
        
        for(int x = 0; x < array.count; x++){
            
            PFObject *tempActiveCode = [PFObject objectWithClassName:@"ActivePromoCode"];
            tempActiveCode = [array objectAtIndex:x];
            NSDate *expirationDate1 = [[NSDate alloc] init];
            expirationDate1 = tempActiveCode[@"expirationDate"];
            
            NSLog(@"Depart Times: %@", expirationDate1);
            if([expirationDate1 compare: [NSDate date]] == NSOrderedAscending || (expirationDate1 == [NSDate date])){
                
                NSLog(@"TEMPACTIVECODETODELETE: %@", tempActiveCode);
                [tempActiveCode deleteInBackground];
                [array removeObjectAtIndex:x];
                
            }
        }
        
    }];
    
    
}



-(void) getActivePromoCodesFromParse{
    
    PFQuery *query = [PFQuery queryWithClassName:@"ActivePromoCode"];
    NSArray *objects = [query findObjects];
    PFObject *activeCodeTemp = [PFObject objectWithClassName:@"ActivePromoCode"];
    
    if(objects){
        
        NSLog(@"Objects: %@", objects);
        for (int x = 0; x < objects.count; x++) {
            
            activeCodeTemp = [objects objectAtIndex:x];
            PFQuery *query = [PFQuery queryWithClassName:@"PromoCode"];
            [query whereKey:@"objectId" equalTo:activeCodeTemp[@"promoCodeId"]];
            NSArray *temp = [query findObjects];
            
            [self.activePromos addObject:[temp objectAtIndex:0]];
        }
        
        if(self.activePromos.count > 0){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Discount Promos" message:@"Before placing your order make sure to check the Promos as there are new discounts available to apply to your order today!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Promo Error" message:@"We had issues pulling the promo codes from the database. Please check your connection and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    
    
}


-(BOOL) prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    [self.delegate flipsideViewControllerDidFinish:self];
}


-(void) setTruckCoord: (CLLocationCoordinate2D) truckLocal{
    
    NSLog(@"Got HERE with Data: %f", truckLocal.latitude);
    self.truckLocationCoord = truckLocal;

}

- (IBAction)navigateToTruck:(id)sender {
    
    
    MKPlacemark *place = [[MKPlacemark alloc] initWithCoordinate:self.truckLocationCoord addressDictionary:nil];
    
    MKMapItem *item =
    [[MKMapItem alloc]initWithPlacemark:place];
    
    
    NSDictionary *options = [[NSDictionary alloc] init];
    options = @{
                MKLaunchOptionsDirectionsModeKey:
                    MKLaunchOptionsDirectionsModeDriving
                };
    
    [item performSelectorOnMainThread:@selector(openInMapsWithLaunchOptions:) withObject:options waitUntilDone:YES];
    
    
}

- (void) webViewDidStartLoad:(UIWebView *)webView{
 
    NSLog(@"StartingWebView");
    
}


- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [spinner stopAnimating];
    NSLog(@"Finished Loading");
    
}



- (IBAction)promoCodeClicked:(id)sender {
    
    if(self.activePromos.count > 0){
        
        self.blackOut = [[UIView alloc] initWithFrame:self.view.frame];
        [self.blackOut setBackgroundColor:[UIColor blackColor]];
        [self.blackOut setAlpha:0.4];
        [self.view addSubview:self.blackOut];
        [self.view bringSubviewToFront:self.blackOut];
        
        
        [UIView transitionWithView:self.container
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromBottom
                        animations:^{
                            
                            [self setUpPromoView];
                            [self.container addSubview: self.promoView];
                            [self.view bringSubviewToFront:self.container];
                            
                        }
                        completion:NULL];
        
        
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Promos" message:@"At this time there are no active discounts that you can apply to your order." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }
    
    
    
}

//Removes Promo Code Window
- (void)promoDoneCodeClicked {
    
    for(UIView *view in promoView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }

    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.blackOut removeFromSuperview];
                        [self.view sendSubviewToBack:self.promoView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
}




-(void) setUpPromoView {
    
    [self.promoView setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:0.99]];
    [self.promoView.layer setCornerRadius:8.0f];
    [self.promoView.layer setBorderColor:[UIColor darkGrayColor].CGColor];
    [self.promoView.layer setBorderWidth:1.5f];
    
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 10, 280, 200) style: UITableViewStylePlain];
    [tableView setBackgroundColor:[UIColor clearColor]];
    tableView.delegate = self;
    tableView.dataSource = self;
    
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(80, 310, 130, 40)];
    [doneButton setTitle:@"DONE" forState:UIControlStateNormal];
    [doneButton setBackgroundColor:[UIColor colorWithRed:(238/255.f) green:(101/255.f) blue: (57/255.f) alpha:1.0]];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(promoDoneCodeClicked) forControlEvents:UIControlEventTouchUpInside];
    
    UITextView *infoView = [[UITextView alloc] initWithFrame:CGRectMake(5, 217, 280, 75)];
    [infoView setText:@"These codes can be used to get a discount on your order at APMV today! Just type one of these codes into the promo section of the order form to apply your discount."];
    [infoView setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:0.99]];
    [infoView setTextColor:[UIColor darkGrayColor]];
    [infoView setUserInteractionEnabled:NO];
    [infoView setTextAlignment:NSTextAlignmentCenter];
    
    
    
    
    [self.promoView addSubview: infoView];
    [self.promoView addSubview:doneButton];
    [self.promoView addSubview:tableView];
    
    
    
}


#pragma mark TABELVIEW DELEGATE

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }

    
    PFObject *tempPromo = [PFObject objectWithClassName:@"PromoCode"];
    tempPromo = [self.activePromos objectAtIndex:indexPath.row];
    cell.textLabel.text = tempPromo[@"discount"];
    
    
    [cell setBackgroundColor:[UIColor clearColor]];

    
    UILabel *codeLabel = [[UILabel alloc] initWithFrame:CGRectMake(150, 5, 127, 34)];
    codeLabel.text = [NSString stringWithFormat:@"Code: %@", tempPromo[@"code"]];

    [cell addSubview:codeLabel];
    
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.activePromos count];
}

@end

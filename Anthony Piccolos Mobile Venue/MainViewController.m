//
//  MainViewController.m
//  Anthony Piccolos Mobile Venue
//
//  Created by ActivateInnovation on 6/4/14.
//  Copyright (c) 2014 ActivateInnovation. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController
@synthesize truckLocation;
@synthesize truckLocationCoord;
@synthesize mapItem;
@synthesize zoomCode;
@synthesize zoomToTruckOutlet;
@synthesize geocoder;
@synthesize placemark;
@synthesize currentAddress;
@synthesize truckAddressLabel;
@synthesize spinner;
@synthesize refreshOutlet;




-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];

    
    
    self.geocoder = [[CLGeocoder alloc] init];
    self.currentAddress = [[NSString alloc] init];
    self.truckLocation = [PFObject objectWithClassName:@"TruckLocation"];
    //  self.placemark = [[CLPlacemark alloc] init];
    self.zoomCode = 1;
 
     [self.mapView setDelegate:self];
    self.refreshOutlet.hidden = YES;
   

     [self getTruckLocation];
}


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background.png"]];
    [background setFrame: self.view.frame];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    
    [self zoomMapViewToFitAnnotations:self.mapView animated:YES];
    [self.mapView setShowsUserLocation:YES];
    
    [self updateActivePromoCodes];
    /*UIView *statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
     [statusView setBackgroundColor:[UIColor lightGrayColor]];
     [self.view addSubview:statusView];
     [self.view sendSubviewToBack:statusView];*/
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.spinner.frame = CGRectMake(135, 415, 50, 50);
    [self.view addSubview:self.spinner];
    [self.view bringSubviewToFront:self.spinner];
    
    
  
    
	// Do any additional setup after loading the view, typically from a nib.
}



-(void) updateActivePromoCodes {
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"ActivePromoCode"];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [array addObjectsFromArray:objects];
        
        for(int x = 0; x < array.count; x++){
            
            PFObject *tempActiveCode = [PFObject objectWithClassName:@"ActivePromoCode"];
            tempActiveCode = [array objectAtIndex:x];
            NSDate *expirationDate1 = [[NSDate alloc] init];
            expirationDate1 = tempActiveCode[@"expirationDate"];
            
            NSLog(@"Depart Times: %@", expirationDate1);
            if([expirationDate1 compare: [NSDate date]] == NSOrderedAscending || (expirationDate1 == [NSDate date])){
                
                NSLog(@"TEMPACTIVECODETODELETE: %@", tempActiveCode);
                [tempActiveCode deleteInBackground];
                [array removeObjectAtIndex:x];
                
            }
        }
        
    }];
    
    
}


-(void) viewDidAppear:(BOOL)animated{
    
    

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Flipside View

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void) getTruckLocation{
    
    [self.spinner stopAnimating];
    // self.refreshOutlet.hidden = YES;
    
    PFQuery *query = [PFQuery queryWithClassName:@"TruckLocation"];
        [query orderByDescending:@"createdAt"];
        
        
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        NSLog(@"TruckLocationObjects : %@", objects);
       //array is used as the mutable form of objects returned from parse
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [array addObjectsFromArray:objects];
        
        for(int x = 0; x < array.count; x++){
        
            PFObject *tempLocation = [PFObject objectWithClassName:@"TruckLocation"];
            tempLocation = [array objectAtIndex:x];
            NSDate *departTime = [[NSDate alloc] init];
            departTime = tempLocation[@"departTime"];
            
            NSLog(@"Depart Times: %@", departTime);
            if([departTime compare: [NSDate date]] == NSOrderedAscending || (departTime == [NSDate date])){
                
                NSLog(@"TEMPLOCATIONTODELETE: %@", tempLocation);
                [tempLocation deleteInBackground];
                [array removeObjectAtIndex:x];
                
            }
        }
        

        
        if(error){
            
            [self zoomMapViewToFitAnnotations: self.mapView animated:YES];
            [self.truckAddressLabel setText:@"Anthony Piccolos Mobile Venue is not accepting online orders at this time. Please check back with us later, or click refresh to check for updates."];
            self.zoomToTruckOutlet.hidden = YES;
            self.navigateOutlet.hidden = YES;
            self.placeOrderOutlet.hidden = YES;
            self.refreshOutlet.hidden = NO;
            
        }
        
        
        if(array.count > 0){
            
            
            self.truckLocation = array[0];
          //  NSLog(@"Truck Location:  %@", self.truckLocation);
            
            PFGeoPoint *geoPoint = self.truckLocation[@"Location"];
            self.truckLocationCoord = CLLocationCoordinate2DMake(geoPoint.latitude, geoPoint.longitude);
            
           // MKCoordinateRegion mapRegion;
            //mapRegion.center = coordinate;
           
            //[self.mapView setRegion:mapRegion animated: YES];
            NSDate *pickerDate = self.truckLocation[@"departTime"];
            NSLog(@"DATE: %@", pickerDate);
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"hh:mm a"];
            NSString *timeString = [formatter stringFromDate:pickerDate];
           
            self.refreshOutlet.hidden = YES;
            [self.truckAddressLabel setText:[NSString stringWithFormat:@"Departing at: %@\nfrom\n%@", timeString, self.truckLocation[@"CurrentAddress"]]];
            self.zoomToTruckOutlet.hidden = NO;
            self.navigateOutlet.hidden = NO;
            self.placeOrderOutlet.hidden = NO;

            
            MKPointAnnotation *truckPin = [[MKPointAnnotation alloc] init];
            [truckPin setTitle:@"Piccolos Mobile Venue"];
            truckPin.coordinate = self.truckLocationCoord;
            [self.mapView addAnnotation:truckPin];
            
            
           /* MKUserLocation *userLocation = self.mapView.userLocation;

            MKPointAnnotation *currentLocation = [[MKPointAnnotation alloc] init];
            [currentLocation setTitle:@"Current Location"];
            currentLocation.coordinate = userLocation.location.coordinate;
            [self.mapView addAnnotation: currentLocation];*/
            
            [self zoomMapViewToFitAnnotations: self.mapView animated:YES];
            [self showPath];
            
           
        }
        else{
            
           // [self.mapView setDelegate:self];
            
           /* MKUserLocation *userLocation = self.mapView.userLocation;

            
            MKPointAnnotation *currentLocation = [[MKPointAnnotation alloc] init];
            [currentLocation setTitle:@"Current Location"];
            currentLocation.coordinate = userLocation.location.coordinate;
            [self.mapView addAnnotation: currentLocation];*/
            self.refreshOutlet.hidden = NO;
            [self zoomMapViewToFitAnnotations: self.mapView animated:YES];
            [self.truckAddressLabel setText:@"Anthony Piccolos Mobile Venue is not accepting online orders at this time. Please check back with us later, or click refresh to check for updates."];
            self.zoomToTruckOutlet.hidden = YES;
            self.navigateOutlet.hidden = YES;
            self.placeOrderOutlet.hidden = YES;
        
        }
       
        
    }];
    
}


-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation{
    
    MKAnnotationView *pinView = nil;
    

    NSLog(@"Annotation: %f, MapView: %f", [annotation coordinate].latitude, self.mapView.userLocation.location.coordinate.latitude);
   
    if([annotation coordinate].latitude != _mapView.userLocation.location.coordinate.latitude && [annotation coordinate].longitude != _mapView.userLocation.location.coordinate.longitude)
    {
        static NSString *defaultPinID;
        pinView = (MKAnnotationView *)[_mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            
            NSLog(@"INNER");
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
       // pinView.canShowCallout = YES;
        pinView.image = [UIImage imageNamed:@"truck.png"];
    }
    else {
        
        NSLog(@"OUTER");
       /* static NSString *defaultPinID;
        pinView = (MKAnnotationView *)[_mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            
        pinView = [[MKAnnotationView alloc]
                   initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        //pinView.canShowCallout = YES;
        pinView.image = [UIImage imageNamed:@"person.png"];*/
       
   
    }
    return pinView;

}


#define MINIMUM_ZOOM_ARC 0.0010 //approximately 1 miles (1 degree of arc ~= 69 miles)
#define ANNOTATION_REGION_PAD_FACTOR 1.3
#define MAX_DEGREES_ARC 360
- (void)zoomMapViewToFitAnnotations:(MKMapView *)mapView animated:(BOOL)animated
{
    NSArray *annotations = mapView.annotations;
    int count = [mapView.annotations count];
    if ( count == 0) { return; } //bail if no annotations
    
    //convert NSArray of id <MKAnnotation> into an MKCoordinateRegion that can be used to set the map size
    //can't use NSArray with MKMapPoint because MKMapPoint is not an id
    MKMapPoint points[count]; //C array of MKMapPoint struct
    for( int i=0; i<count; i++ ) //load points C array by converting coordinates to points
    {
        CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)[annotations objectAtIndex:i] coordinate];
        points[i] = MKMapPointForCoordinate(coordinate);
    }
    //create MKMapRect from array of MKMapPoint
    MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:count] boundingMapRect];
    //convert MKCoordinateRegion from MKMapRect
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
    
    //add padding so pins aren't scrunched on the edges
    region.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
    region.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
    //but padding can't be bigger than the world
    if( region.span.latitudeDelta > MAX_DEGREES_ARC ) { region.span.latitudeDelta  = MAX_DEGREES_ARC; }
    if( region.span.longitudeDelta > MAX_DEGREES_ARC ){ region.span.longitudeDelta = MAX_DEGREES_ARC; }
    
    //and don't zoom in stupid-close on small samples
    if( region.span.latitudeDelta  < MINIMUM_ZOOM_ARC ) { region.span.latitudeDelta  = MINIMUM_ZOOM_ARC; }
    if( region.span.longitudeDelta < MINIMUM_ZOOM_ARC ) { region.span.longitudeDelta = MINIMUM_ZOOM_ARC; }
    //and if there is a sample of 1 we want the max zoom-in instead of max zoom-out
    if( count == 1 )
    {
        region.span.latitudeDelta = MINIMUM_ZOOM_ARC;
        region.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    }
    [mapView setRegion:region animated:animated];
}

- (IBAction)ZoomtoTruck:(id)sender {
    
    
    if(zoomCode == 1){
      
        MKCoordinateRegion mapRegion;
        mapRegion.center = self.truckLocationCoord;
        
        [self.mapView setRegion:mapRegion animated: YES];
        self.zoomCode = 2;
          [self.zoomToTruckOutlet setTitle:@"Zoom Out" forState:UIControlStateNormal];
    }
    else{
        self.zoomCode = 1;
        [self.zoomToTruckOutlet setTitle:@"Zoom to Truck" forState:UIControlStateNormal];
        [self zoomMapViewToFitAnnotations:self.mapView animated: YES];
        
        
    }
   
    
}


-(void) showPath{

    
    MKPlacemark *place = [[MKPlacemark alloc]
                          initWithCoordinate: self.truckLocationCoord
                          addressDictionary:nil];
    
    self.mapItem =
    [[MKMapItem alloc]initWithPlacemark:place];
    
    [self.mapView setHidden:NO];
    
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    [request setSource:[MKMapItem mapItemForCurrentLocation]];
    [request setDestination:self.mapItem];
    [request setTransportType:MKDirectionsTransportTypeAny]; // This can be limited to automobile and walking directions.
    [request setRequestsAlternateRoutes:YES]; // Gives you several route options.
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        if (!error) {
            MKRoute *route = [response routes][0];
            [self.mapView addOverlay:[route polyline] level:MKOverlayLevelAboveRoads];
           // [self.mapView setVisibleMapRect:[[route polyline]boundingMapRect] animated:YES];
            
        }
    }];
    
    
    
    
}


- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay: (id<MKOverlay> )overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        [renderer setStrokeColor:[UIColor blueColor]];
        [renderer setLineWidth:3.0];
        
        
        
        return renderer;
    }
    return nil;
}


/*- (IBAction)navigate:(id)sender {
    
  //  CLLocation *truckLocal = [[CLLocation alloc] initWithLatitude: self.truckLocationCoord.latitude longitude: self.truckLocationCoord.longitude];
    
    
    //self.placemark = [[CLPlacemark alloc] init];
    [self.geocoder reverseGeocodeLocation: truckLocal completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks");
        if (error == nil && [placemarks count] > 0) {
           
            self.placemark = [[CLPlacemark alloc] initWithPlacemark:[placemarks lastObject]];
            NSString *text = [NSString stringWithFormat:@"%@ %@\n%@, %@ %@", placemark.subThoroughfare, placemark.thoroughfare, placemark.locality, placemark.administrativeArea, placemark.postalCode];
            
            self.currentAddress = [[NSString alloc] initWithString:text];
            NSLog(@"Current Truck Address: %@", text);
    
            
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    } ];

   // MKPlacemark *place = [[MKPlacemark alloc] initWithPlacemark:self.placemark];
    MKPlacemark *place = [[MKPlacemark alloc] initWithCoordinate:self.truckLocationCoord addressDictionary:nil];
    
    MKMapItem *item =
    [[MKMapItem alloc]initWithPlacemark:place];
    
    
    NSDictionary *options = [[NSDictionary alloc] init];
                options = @{
                              MKLaunchOptionsDirectionsModeKey:
                                  MKLaunchOptionsDirectionsModeDriving
                              };
    
    [item performSelectorOnMainThread:@selector(openInMapsWithLaunchOptions:) withObject:options waitUntilDone:YES];
    
    
}*/

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    NSLog(@"YESS");
    
}
- (IBAction)refreshInformation:(id)sender {
    self.refreshOutlet.hidden = NO;
    [self.truckAddressLabel setText:@""];
    [self.spinner startAnimating];
    [self performSelector:@selector(getTruckLocation) withObject:nil afterDelay:0.5];
    
    
    
}
- (IBAction)placeOrder:(id)sender {
    
    PFObject *tempObject = [PFObject objectWithClassName:@"TruckLocation"];
    PFQuery *query = [PFQuery queryWithClassName:@"TruckLocation"];
   tempObject = [query getFirstObject];
    
    if(tempObject){
        
        NSDate *startDate = [NSDate date];
        
     
       // NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setDateFormat:@"hh:mm"];
        
      //  NSDate *endDate = [dateFormatter dateFromString: tempObject[@"departTime"]];
        NSDate *endDate = tempObject[@"departTime"];
        
        NSCalendar *gregorian = [[NSCalendar alloc]
                                 initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        
        NSUInteger unitFlags = NSCalendarUnitMinute;
        
        NSDateComponents *components = [gregorian components:unitFlags
                                                    fromDate:startDate
                                                      toDate:endDate options: 0];
               NSInteger minute = [components minute];
        NSLog(@"Minutes between %d", minute);
        
        
        if(minute < 5){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Distance Error" message:@"We are sorry, the food truck will be leaving from its location before you would be able to get there and pick up an order. Please check back at another time when the food truck is open." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else{
        
            UIStoryboard *storyboard;
            if(self.view.frame.size.width > 320){
                
                storyboard = [UIStoryboard storyboardWithName:@"Main-IPad" bundle:nil];
            }
            else{
                
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            }
        FlipsideViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"placeOrder"];
        [viewController setTruckLocationCoord:self.truckLocationCoord];
        [self presentViewController: viewController animated:YES completion:nil];
            
        }
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Truck Closed" message:@"We are sorry, our food truck is not accepting online orders at this time, please check back with us later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [self.mapView removeAnnotations:self.mapView.annotations];
        [self getTruckLocation];
        
    }
    
    
}


@end

//
//  MainViewController.h
//  Anthony Piccolos Mobile Venue
//
//  Created by ActivateInnovation on 6/4/14.
//  Copyright (c) 2014 ActivateInnovation. All rights reserved.
//
#import <MapKit/MapKit.h>
#import "FlipsideViewController.h"
#import <Parse/Parse.h>



@interface MainViewController : UIViewController <FlipsideViewControllerDelegate, MKMapViewDelegate, UIAlertViewDelegate>



@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) PFObject *truckLocation;
- (IBAction)ZoomtoTruck:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *zoomToTruckOutlet;
@property (nonatomic) CLLocationCoordinate2D truckLocationCoord;
@property (strong, nonatomic) MKMapItem *mapItem;
@property int zoomCode;
//- (IBAction)navigate:(id)sender;
@property (strong, nonatomic) CLGeocoder *geocoder;
@property (strong, nonatomic) CLPlacemark *placemark;
@property (strong, nonatomic) NSString *currentAddress;
@property (strong, nonatomic) IBOutlet UILabel *truckAddressLabel;
@property (strong, nonatomic) IBOutlet UIButton *navigateOutlet;
@property (strong, nonatomic) IBOutlet UIButton *placeOrderOutlet;
-(void) getTruckLocation;
- (IBAction)placeOrder:(id)sender;
- (IBAction)refreshInformation:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *refreshOutlet;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@end

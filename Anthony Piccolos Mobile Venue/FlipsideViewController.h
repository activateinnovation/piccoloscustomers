//
//  FlipsideViewController.h
//  Anthony Piccolos Mobile Venue
//
//  Created by ActivateInnovation on 6/4/14.
//  Copyright (c) 2014 ActivateInnovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>


@class FlipsideViewController;


@protocol FlipsideViewControllerDelegate

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
@end


@interface FlipsideViewController : UIViewController <UIWebViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) id <FlipsideViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;
-(void) setTruckCoord: (CLLocationCoordinate2D) truckLocal;
@property (nonatomic) CLLocationCoordinate2D truckLocationCoord;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)navigateToTruck:(id)sender;

@property (strong, nonatomic) UIActivityIndicatorView *spinner;
- (IBAction)promoCodeClicked:(id)sender;
@property (strong, nonatomic) NSMutableArray *activePromos;

@property (strong, nonatomic) UIView *container;
@property (strong, nonatomic) UIView *promoView;
@property (strong, nonatomic) UIView *blackOut;
@end
